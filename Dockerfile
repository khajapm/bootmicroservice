FROM openjdk:8-jre-alpine
LABEL maintainer "kpasha@deloitte.com"
COPY target/springbootms.jar springbootms.jar
EXPOSE 8087
ENTRYPOINT ["java","-jar","springbootms.jar"]
